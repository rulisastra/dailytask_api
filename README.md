# dailytask_API
menggunakan goLang
untuk tugas pertemuan 17 >> DTS-IT Perbankan

>>  localhost:5000
    (http://localhost:5000/dailytask)
    

# how to use this repo
install xampp
        mysql
        golang
        git
        IDE

# add existing folder to gitlab
```
cd existing_folder
git init
git remote add origin https://gitlab.com/rulisastra/dailytask_api.git
git add .
git commit -m "Initial commit"
git push -u origin master
```


# open mysql
`mysql -u root `
```
CREATE TABLE IF NOT EXISTS `dailytask` (
  `id` int(11) NOT NULL auto_increment,          
  `name` varchar(250)  NOT NULL default '',     
  `departemen` varchar(30) NOT NULL default '',    
  `task` varchar(100) NOT NULL default '',
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY  (`id`)
);
```
```
ALTER TABLE dailytask
ADD COLUMN status VARCHAR(15) AFTER task;
```
```
INSERT INTO dailytask(id,name,departemen,task,status) VALUES
    (1,'jainudin','backend','add mysql','on going'),
    (2,'sinta','backend','add table','on going'),
    (3,'rian','frontend','design table','done'),
    (4,'jajal','frontend','design figma','on going'),
    (5,'ruli','backend','deploy gitlab','to do');
```

# code, open and running
```
go mod init namapackage
go run ___.go
go get __url__.git
```
```
// untuk dapet file .exe
go install ->> untuk bikin .bin (harus set path env bin manually tiap project)
go build ->> lebih ringkes, tapi kalo ada package import yang blm didownload, bs error
```

# deploy CI/CD with gitlab




