package model

import "time"

type DailyTask struct {
	ID         int       `json:"id"`
	Name       string    `name:"name"`
	Departemen string    `json:"departemen"`
	Task       string    `json:"task"`
	Status     string    `json:"status"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}
