package main

import (
	"dailytask_api/config"
	"dailytask_api/dailytask"
	"dailytask_api/model"
	"dailytask_api/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

const addr = `:5000`

type Server struct {
	db      *sql.DB
	ViewDir string
}
type handler func(w http.ResponseWriter, r *http.Request)

func InitServer() *Server {
	db, err := config.Mysql()
	if err != nil {
		log.Fatal(err)
	}
	return &Server{
		db:      db,
		ViewDir: `views/`,
	}
}
func (s *Server) Listen() {
	log.Println(`listen at ` + addr)
	http.HandleFunc(`/dailytask`, s.DailyTask()) // by staff and supervisor
	http.HandleFunc(`/dailytask/create`, s.DailyTaskCreate()) // by supervisor only
	http.HandleFunc(`/dailytask/update`, s.DailyTaskUpdate()) // by staff only
	err := http.ListenAndServe(addr, nil)
	if err != nil {
		fmt.Println(err)
	}
}
func (s *Server) DailyTask() handler {
	return func(w http.ResponseWriter, r *http.Request) {
		dailytasks, err := dailytask.SelectAll(s.db)
		if utils.IsError(w, err) {
			return
		}
		utils.ResponseJson(w, dailytasks)
	}
}
func (s *Server) DailyTaskCreate() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == `GET` {
			http.ServeFile(w, r, s.ViewDir+`dailytask_create.html`)
			return
		}
		m := model.DailyTask{}
		err := json.NewDecoder(r.Body).Decode(&m)
		if utils.IsError(w, err) {
			return
		}
		err = dailytask.Insert(s.db, &m)
		if utils.IsError(w, err) {
			return
		}
		utils.ResponseJson(w, m)
	}
}
func (s *Server) DailyTaskUpdate() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == `GET` {
			http.ServeFile(w, r, s.ViewDir+`dailytask_update.html`)
			return
		}
		m := model.DailyTask{}
		err := json.NewDecoder(r.Body).Decode(&m)
		if utils.IsError(w, err) {
			return
		}
		affected, err := dailytask.Update(s.db, &m)
		if utils.IsError(w, err) {
			return
		}
		res := map[string]interface{}{}
		res[`affected`] = affected
		utils.ResponseJson(w, res)
	}
}

func main() {
	server := InitServer()
	server.Listen()
}
